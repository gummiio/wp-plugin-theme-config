<?php

namespace Gummiforweb\ThemeBuilder\Loader;

class Taxonomy
{
    public function __construct()
    {
        add_action('init', [$this, 'registerTaxonomies']);
    }

    public function registerTaxonomies()
    {
        if (! $taxonomies = theme_config('custom_taxonomies')) {
            return false;
        }

        collect($taxonomies)->each(function($options, $slug) {
            if (taxonomy_exists($slug)) {
                $options = wp_parse_args($options, $this->getTaxonomyObjectArray($slug));
            }

            register_taxonomy($slug, $this->preparePostTypes($options), $this->prepareOptions($options));
        });
    }

    protected function getTaxonomyObjectArray($slug)
    {
        $taxonomy = json_decode(json_encode(get_taxonomy($slug)), true);
        $taxonomy['singular'] = data_get($taxonomy, 'labels.singular_name');
        $taxonomy['plural'] = data_get($taxonomy, 'labels.name');
        $taxonomy['url_slug'] = data_get($taxonomy, 'rewrite.slug', '');

        unset($taxonomy['cap']); // bug, need to let wp regenerate

        return $taxonomy;
    }

    protected function preparePostTypes($options)
    {
        $postTypes = array_wrap(data_get($options, 'post_types', []));

        if (! $objectTypes = data_get($options, 'object_type')) {
            return $postTypes;
        }

        return array_merge($postTypes, $objectTypes);
    }

    protected function prepareOptions($options)
    {
        return wp_parse_args($options, [
            'labels'            => $this->prepareLabels($options),
            'show_admin_column' => true,
            'hierarchical'      => true,
            'rewrite'           => [
                'slug' => data_get($options, 'url_slug', '')
            ]
        ]);
    }

    protected function prepareLabels($options)
    {
        $singular = $options['singular'];
        $plural = $options['plural'];

        return [
            'name'                       => __($plural),
            'singular_name'              => __($singular),
            'search_items'               => __('Search ' . $plural),
            'popular_items'              => __('Popular ' . $plural),
            'all_items'                  => __('All ' . $plural),
            'parent_item'                => __('Parent ' . $singular),
            'parent_item_colon'          => __('Parent ' . $singular . ':'),
            'edit_item'                  => __('Edit ' . $singular),
            'view_item'                  => __('View ' . $singular),
            'update_item'                => __('Update ' . $singular),
            'add_new_item'               => __('Add New ' . $singular),
            'new_item_name'              => __('New ' . $singular . ' Name'),

            'separate_items_with_commas' => __('Separate ' . $plural . ' with commas'),
            'add_or_remove_items'        => __('Add or remove ' . $plural),
            'choose_from_most_used'      => __('Choose from the most used ' . $plural),
            'not_found'                  => __('No ' . $plural . ' found.'),
            'no_terms'                   => __('No ' . $plural),
            'items_list_navigation'      => __($plural . ' list navigation'),
            'items_list'                 => __($plural . ' list'),
        ];
    }
}
