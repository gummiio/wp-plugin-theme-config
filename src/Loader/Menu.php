<?php

namespace Gummiforweb\ThemeBuilder\Loader;

class Menu
{
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'registerMenus']);
    }

    public function registerMenus()
    {
        register_nav_menus(theme_config('menus', []));
    }
}
