<?php

namespace Gummiforweb\ThemeBuilder\Loader;

class PostType
{
    public function __construct()
    {
        add_action('init', [$this, 'registerPostTypes']);
    }

    public function registerPostTypes()
    {
        if (! $postTypes = theme_config('custom_post_types')) {
            return false;
        }

        collect($postTypes)->each(function($options, $slug) {
            if (post_type_exists($slug)) {
                $options = wp_parse_args($options, $this->getPostTypeObjectArray($slug));
            }

            register_post_type($slug, $this->prepareOptions($options));
        });
    }

    protected function getPostTypeObjectArray($slug)
    {
        $postType = json_decode(json_encode(get_post_type_object($slug)), true);
        $postType['singular'] = data_get($postType, 'labels.singular_name');
        $postType['plural'] = data_get($postType, 'labels.name');
        $postType['url_slug'] = data_get($postType, 'rewrite.slug', '');

        unset($postType['cap']); // bug, need to let wp regenerate

        return $postType;
    }

    protected function prepareOptions($options)
    {
        return wp_parse_args($options, [
            'labels'        => $this->prepareLabels($options),
            'public'        => true,
            'menu_position' => 46,
            'supports'      => ['title', 'editor', 'thumbnail', 'revisions', 'excerpt'],
            'rewrite'       => [
                'slug' => data_get($options, 'url_slug', '')
            ],
        ]);
    }

    protected function prepareLabels($options)
    {
        $singular = $options['singular'];
        $plural = $options['plural'];

        return wp_parse_args(data_get($options, 'labels', []), [
            'name'                  => __($plural),
            'singular_name'         => __($singular),
            'add_new'               => __('Add New'),
            'add_new_item'          => __('Add New ' . $singular),
            'edit_item'             => __('Edit ' . $singular),
            'new_item'              => __('New ' . $singular),
            'view_item'             => __('View ' . $singular),
            'view_items'            => __("View $plural"),
            'search_items'          => __('Search ' . $singular),
            'not_found'             => __('Nothing found in the Database.'),
            'not_found_in_trash'    => __('Nothing found in Trash'),
            'parent_item_colon'     => __('Parent ' . $singular . ':'),

            'all_items'             => __('All ' . $plural),
            'archives'              => __($singular . ' Archives'),
            'attributes'            => __("$singular Attributes"),

            'insert_into_item'      => __('Insert into ' . $singular),
            'uploaded_to_this_item' => __('Uploaded to this ' . $singular),

            'featured_image'        => __('Featured Image'),
            'set_featured_image'    => __('Set featured image'),
            'remove_featured_image' => __('Remove featured image'),
            'use_featured_image'    => __('Use as featured image'),

            'filter_items_list'     => __('Filter ' . $plural . ' list'),
            'items_list_navigation' => __($plural . ' list navigation'),
            'items_list'            => __($plural . ' list'),
        ]);
    }
}
