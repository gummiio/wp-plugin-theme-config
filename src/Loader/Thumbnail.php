<?php

namespace Gummiforweb\ThemeBuilder\Loader;

class Thumbnail
{
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'registerThumbnailSizes'], 15);
    }

    public function registerThumbnailSizes()
    {
        collect(theme_config('thumbnail_sizes', []))->each(function($sizes, $name) {
            if ($sizes === false) {
                remove_image_size($name);
                return true;
            }

            if (is_string($sizes)) {
                $sizes = array_map('trim', explode(',', $sizes));
            }

            array_unshift($sizes, $name);
            call_user_func_array('add_image_size', $sizes);
        });
    }
}
