<?php

namespace Gummiforweb\ThemeBuilder\Loader;

class Script
{
    public function __construct()
    {
        add_action('wp_head', [$this, 'addTypekitScript']);
        add_action('wp_head', [$this, 'addPingbackScript']);

        add_action('wp_enqueue_scripts', [$this, 'switchjQuery']);
        add_action('wp_enqueue_scripts', [$this, 'registerGoogleFont']);
        add_action('wp_enqueue_scripts', [$this, 'registerFontAwesome']);
        add_action('wp_enqueue_scripts', [$this, 'registerTypekitScripts']);
        add_action('wp_enqueue_scripts', [$this, 'registerFrontendScripts'], 15);

        add_action('admin_enqueue_scripts', [$this, 'registerFontAwesome']);
        add_action('admin_enqueue_scripts', [$this, 'registerAdminScripts']);
    }

    public function addTypekitScript()
    {
        if (! $typekitId = theme_config('typekit.id')) {
            return;
        }

        if (theme_config('typekit.use_style')) {
            return;
        }

        printf('
            <script src="https://use.typekit.net/%s.js"></script>
            <script>try{Typekit.load({async: true});}catch(e){}</script>
            %s
        ', $typekitId, "\n");
    }

    public function addPingbackScript()
    {
        if (is_singular() && pings_open(get_queried_object())) {
            printf('<link rel="pingback" href="%s">', get_bloginfo('pingback_url'));
        }
    }

    public function switchjQuery()
    {
        if (! theme_config('jquery')) {
            return;
        }

        wp_deregister_script('jquery');
        $this->enqueueScript('jquery', theme_config('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'), ['footer' => false]);
    }

    public function registerGoogleFont()
    {
        if (! $googleFonts = theme_config('googlefonts')) {
            return;
        }

        $this->enqueueStyle('theme-googlefonts', $googleFonts);
    }

    public function registerFontAwesome()
    {
        if (! $fontawesome = theme_config('fontawesome')) {
            return;
        }

        $this->enqueueStyle('theme-fontawesome', $fontawesome);
    }

    public function registerTypekitScripts()
    {
        if (! $typekitId = theme_config('typekit.id')) {
            return;
        }

        if (! theme_config('typekit.use_style')) {
            return;
        }

        $this->enqueueStyle('typekit', sprintf('//use.typekit.net/%s.css', $typekitId));
    }

    public function registerFrontendScripts()
    {
        collect(theme_config('frontend_scripts', []))->each(function($src, $name) {
            $arguments = is_array($src)? $src : [];
            $src = is_array($src)? data_get($src['source']) : $src;
            $this->enqueueScript($name, $src, $arguments);
        });

        collect(theme_config('frontend_styles', []))->each(function($src, $name) {
            $arguments = is_array($src)? $src : [];
            $src = is_array($src)? data_get($src['source']) : $src;
            $this->enqueueStyle($name, $src, $arguments);
        });
    }

    public function registerAdminScripts()
    {
        collect(theme_config('admin_scripts', []))->each(function($src, $name) {
            $arguments = is_array($src)? $src : [];
            $src = is_array($src)? data_get($src['source']) : $src;
            $this->enqueueScript($name, $src, $arguments);
        });

        collect(theme_config('admin_styles', []))->each(function($src, $name) {
            $arguments = is_array($src)? $src : [];
            $src = is_array($src)? data_get($src['source']) : $src;
            $this->enqueueStyle($name, $src, $arguments);
        });
    }

    protected function enqueueStyle($name, $source, $arguments = [])
    {
        $arguments = $this->adjustArgumentsBySource(wp_parse_args($arguments, [
            'name' => $name,
            'source' => $source,
            'deps' => [],
            'version' => null,
            'media' => 'all'
        ]));

        if (! $arguments) {
            return;
        }

        call_user_func_array('wp_enqueue_style', collect($arguments)->values()->all());
    }

    protected function enqueueScript($name, $source, $arguments = [])
    {
        $arguments = $this->adjustArgumentsBySource(wp_parse_args($arguments, [
            'name' => $name,
            'source' => $source,
            'deps' => [],
            'version' => null,
            'footer' => true
        ]));

        if (! $arguments) {
            return;
        }

        call_user_func_array('wp_enqueue_script', collect($arguments)->values()->all());
    }

    protected function adjustArgumentsBySource($arguments)
    {
        if (! $this->isExternalSource($arguments['source'])) {
            $source = resolve_config_url($arguments['source']);

            if (! is_file($source['path'])) {
                return false;
            }

            $arguments['source'] = $source['url'];
            $arguments['version'] = filemtime($source['path']);
        }

        return $arguments;
    }

    protected function isExternalSource($source)
    {
        return strpos($source, '//') === 0 || strpos($source, 'http') === 0;
    }
}
