<?php

if (! function_exists('resolve_config_url')) {
    function resolve_config_url($value) {
        $url = get_stylesheet_directory_uri();
        $path = get_stylesheet_directory();

        if (strpos($value, '^') === 0) {
            $url = get_template_directory_uri();
            $path = get_template_directory();
            $value = substr($value, 1);
        }

        return [
            'url' => $url .= '/' . trim($value, '/\\'),
            'path' => $path .= '/' . trim($value, '/\\')
        ];
    }
}

if (! function_exists('array_wrap')) {
    function array_wrap($value) {
        return is_array($value)? $value : [$value];
    }
}

function theme_builder($library = null) {
    global $themeBuilder;

    return $library? data_get($themeBuilder, $library, null) : $themeBuilder;
}

function theme_config($key = '*', $default = null) {
    return theme_builder('config')->get($key, $default);
}
