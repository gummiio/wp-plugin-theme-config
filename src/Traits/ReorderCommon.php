<?php

namespace Gummiforweb\ThemeBuilder\Traits;

trait ReorderCommon
{
    protected function getTaxonomyPostTypes($taxonomies)
    {
        $postTypes = [];

        foreach ($taxonomies as $taxonomy) {
            if (! $taxonomy = get_taxonomy($taxonomy)) {
                continue;
            }

            $postTypes = array_merge($postTypes, data_get($taxonomy, 'object_type', []));
        }

        return collect($postTypes)->unique()->values()->all();
    }

    protected function filterConfigValue($original, $configKey, $default)
    {
        $values = $this->maybeToArray(theme_config($configKey, $default));

        if (in_array('*', $values)) {
            return $original;
        }

        return array_intersect($original, $values);
    }

    protected function maybeToArray($value)
    {
        if (is_array($value)) {
            return $value;
        }

        if (is_string($value)) {
            return array_map('trim', explode(',', $value));
        }

        return [];
    }

    protected function managedByConfig()
    {
        return theme_config('reorder.managed_by_config');
    }

    protected function isTrueAdmin()
    {
        return is_admin() && ! $this->isAjax();
    }

    protected function isAjax()
    {
        return defined('DOING_AJAX') && DOING_AJAX;
    }

    protected function hasMultiplePostTypes($query)
    {
        $postTypes = array_wrap($query->get('post_type'));

        return in_array('any', $postTypes) || count($postTypes) > 1;
    }

    protected function hasTaxonomyQuery($query)
    {
        return $this->hasCategoryQuery($query) || $this->hasTagQuery($query) || $this->hasCustomTaxonomyQuery($query);
    }

    protected function hasCategoryQuery($query)
    {
        return array_intersect_key(
            array_flip(['cat', 'category_name', 'category__and', 'category__in', 'category__not_in']),
            array_filter($query->query_vars? : [])
        );
    }

    protected function hasTagQuery($query)
    {
        return array_intersect_key(
            array_flip(['tag', 'tag_id', 'tag__and', 'tag__in', 'tag__not_in', 'tag_slug__and', 'tag_slug__in']),
            array_filter($query->query_vars? : [])
        );
    }

    protected function hasCustomTaxonomyQuery($query)
    {
        $taxQuery = collect($query->get('tax_query')? : []);

        if ($taxQuery->isEmpty()) {
            return [];
        }

        if ($this->isStandardTaxonomyQuery($taxQuery)) {
            return $taxQuery->reject(function ($value, $key) {
                return $key === 'relation';
            })->all();
        }

        return $taxQuery->flatten(1)->reject(function ($value, $key) {
            return ! is_array($value);
        })->all();
    }

    protected function isStandardTaxonomyQuery($taxQuery)
    {
        $first = $taxQuery->reject(function ($value, $key) {
            return $key === 'relation';
        })->first();

        return array_key_exists('taxonomy', $first) && array_key_exists('field', $first);
    }

    protected function hasOrderQuery($query)
    {
        return !! ($query->get('order') || $query->get('orderby'));
    }

    protected function isReorderActivated()
    {
        return class_exists('MN_Reorder');
    }

    protected function isReorderTermsActivated()
    {
        return class_exists('Reorder_Terms');
    }

    protected function isReorderByTermActivated()
    {
        return class_exists('Reorder_By_Term');
    }
}
