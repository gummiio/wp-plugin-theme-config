<?php

namespace Gummiforweb\ThemeBuilder;

use Gummiforweb\ThemeBuilder\Core\Config;
use Gummiforweb\ThemeBuilder\Core\ThemeUpdate;
use Gummiforweb\ThemeBuilder\Core\Updator;
use Gummiforweb\ThemeBuilder\Enhance\AdminQuickLink;
use Gummiforweb\ThemeBuilder\Enhance\Debugger;
use Gummiforweb\ThemeBuilder\Enhance\Editor;
use Gummiforweb\ThemeBuilder\Enhance\Favicon;
use Gummiforweb\ThemeBuilder\Enhance\HeadMeta;
use Gummiforweb\ThemeBuilder\Enhance\LoginScreen;
use Gummiforweb\ThemeBuilder\Enhance\ThemeSupport;
use Gummiforweb\ThemeBuilder\Loader\Menu;
use Gummiforweb\ThemeBuilder\Loader\PostType;
use Gummiforweb\ThemeBuilder\Loader\Script;
use Gummiforweb\ThemeBuilder\Loader\Taxonomy;
use Gummiforweb\ThemeBuilder\Loader\Thumbnail;
use Gummiforweb\ThemeBuilder\Plugin\Acf;
use Gummiforweb\ThemeBuilder\Plugin\GravityForm;
use Gummiforweb\ThemeBuilder\Plugin\Reorder;
use Gummiforweb\ThemeBuilder\Plugin\ReorderByTerm;
use Gummiforweb\ThemeBuilder\Plugin\ReorderTerms;
use Gummiforweb\ThemeBuilder\Plugin\Yoast;

class ThemeBuilder
{
    protected $file;
    protected $version;

    public function __construct($file, $version)
    {
        $this->file = $file;
        $this->version = $version;

        $this->initialize();
    }

    public function version()
    {
        return $this->version;
    }

    public function file()
    {
        return $this->file;
    }

    public function path($path = '')
    {
        return untrailingslashit(plugin_dir_path($this->file) . untrailingslashit($path));
    }

    public function url($uri = '')
    {
        return untrailingslashit(plugin_dir_url($this->file) . untrailingslashit($uri));
    }

    protected function initialize()
    {
        $this->config         = new Config;
        // $this->updator     = new Updator;
        $this->debugger       = new Debugger;
        $this->themeUpdate    = new ThemeUpdate;
        $this->editor         = new Editor;
        $this->cleanUp        = new HeadMeta;
        $this->postType       = new PostType;
        $this->taxonomy       = new Taxonomy;
        $this->menu           = new Menu;
        $this->thumbnail      = new Thumbnail;
        $this->script         = new Script;
        $this->themeSupport   = new ThemeSupport;
        $this->loginScreen    = new LoginScreen;
        $this->favicon        = new Favicon;
        $this->adminQUickLink = new AdminQuickLink;

        $this->acf            = new Acf;
        $this->gravityForm    = new GravityForm;
        $this->yoast          = new Yoast;
        $this->reorder        = new Reorder;
        $this->reorderByTerm  = new ReorderByTerm;
        $this->reorderTerms   = new ReorderTerms;
    }
}
