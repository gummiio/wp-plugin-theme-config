<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class LoginScreen
{
    public function __construct()
    {
        add_action('login_head', [$this, 'addLogoStyle']);
    }

    public function addLogoStyle()
    {
        if (! $adminLogo = theme_config('admin_logo')) {
            return false;
        }

        $url = get_stylesheet_directory_uri() . '/' . trim($adminLogo, '/\\');
        $path = get_stylesheet_directory() . '/' . trim($adminLogo, '/\\');

        if (! is_file($path)) {
            return false;
        }

        printf(
            '<style type="text/css">
                h1 a{background:0 0!important;height:auto!important;width:100%%!important;text-indent:0!important}
                h1 a img{max-width:240px;width:auto}
            </style>
            <script type="text/javascript">
                window.onload = function(){
                    var aTag = document.getElementById("login").getElementsByTagName("a")[0];
                    aTag.innerHTML = \'<img src="%s" alt="%s">\';
                    aTag.href = "%s";
                    aTag.title = "Go to site";
                }
            </script>',
            $url,
            get_bloginfo('name'),
            home_url()
        );
    }
}
