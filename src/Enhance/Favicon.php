<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class Favicon
{
    public function __construct()
    {
        add_action('wp_head', [$this, 'addFavicons']);
    }

    public function addFavicons()
    {
        $favicons = $this->getFavicons();

        collect($favicons)->each(function($source, $meta) {
            $url = get_stylesheet_directory_uri() . '/' . trim($source, '/\\');
            $path = get_stylesheet_directory() . '/' . trim($source, '/\\');

            if (! is_file($path)) {
                return false;
            }

            printf("<link rel=\"%s\" href=\"%s?v=%s\">\n", $meta, $url, filemtime($path));
        });
    }

    protected function getFavicons()
    {
        $favicons = theme_config('favicons', []);

        if ($favicon = theme_config('favicon')) {
            $favicons['shortcut icon'] = $favicon;
        }

        if ($phoneIcon = theme_config('phone_icon')) {
            $favicons['apple-touch-icon'] = $phoneIcon;
        }

        return $favicons;
    }
}
