<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class Editor
{
    public function __construct()
    {
        add_action('admin_init', [$this, 'loadFontAwesome']);
        add_action('admin_init', [$this, 'loadGoogleFonts']);
        add_action('admin_init', [$this, 'loadEditorStyles'], 20);

        add_action('admin_init', [$this, 'loadTypekitStyle']);
        add_filter('mce_external_plugins', [$this, 'loadTypekitScript']);
        add_filter('tiny_mce_before_init', [$this, 'addTypekitId']);

        add_filter('mce_buttons_2', [$this, 'enableStyleDropdown'], 20);
        add_filter('teeny_mce_buttons', [$this, 'enableStyleDropdown'], 20);
        add_filter('tiny_mce_before_init', [$this, 'addStyleDropdown'], 20);

        add_filter('mce_buttons_2', [$this, 'enableTextColor']);
        add_filter('teeny_mce_buttons', [$this, 'enableTextColor']);
        add_filter('tiny_mce_before_init', [$this, 'addTextColor']);
    }

    public function loadFontAwesome()
    {
        if (! $fontawesome = theme_config('fontawesome')) {
            return;
        }

        $this->addEditorStyle($fontawesome);
    }

    public function loadGoogleFonts()
    {
        if (! $googleFonts = theme_config('googlefonts')) {
            return;
        }

        $this->addEditorStyle($googleFonts);
    }

    public function loadEditorStyles()
    {
        if (! $editorStyles = theme_config('editor_styles')) {
            return;
        }

        collect(array_wrap($editorStyles))->each(function($style) {
            $this->addEditorStyle($style);
        });
    }

    public function loadTypekitStyle()
    {
        if (! $typekitId = theme_config('typekit.id')) {
            return;
        }

        if (! theme_config('typekit.use_style')) {
            return;
        }

        $this->addEditorStyle(sprintf('//use.typekit.net/%s.css', $typekitId));
    }

    public function loadTypekitScript($plugins)
    {
        if (! theme_config('typekit.id')) {
            return $plugins;
        }

        if (theme_config('typekit.use_style')) {
            return $plugins;
        }

        $plugins['theme_builder_typekit'] = theme_builder()->url('/scripts/tinymce.typekit-loader.js');

        return $plugins;
    }

    public function addTypekitId($mceInit)
    {
        if (! $typekitId = theme_config('typekit.id')) {
            return $mceInit;
        }

        if (theme_config('typekit.use_style')) {
            return $mceInit;
        }

        $mceInit['theme_builder_typekit_id'] = $typekitId;

        return $mceInit;
    }

    public function enableStyleDropdown($buttons)
    {
        if (! theme_config('enable_tinymce_additional_styles')) {
            return $buttons;
        }

        array_unshift($buttons, 'styleselect');

        return $buttons;
    }

    public function addStyleDropdown($initArray)
    {
        if (! theme_config('enable_tinymce_additional_styles')) {
            return $initArray;
        }

        $initArray['style_formats'] = json_encode(theme_config('tinymce_additional_styles'));

        return $initArray;
    }

    public function enableTextColor($buttons)
    {
        if (theme_config('enable_tinymce_text_colors')) {
            if (! in_array('forecolor', $buttons)) {
                array_unshift($buttons, 'forecolor');
            }

            return $buttons;
        }

        return collect($buttons)->reject(function($button) {
            return $button == 'forecolor';
        })->all();
    }

    public function addTextColor($initArray)
    {
        if (! theme_config('enable_tinymce_text_colors')) {
            return $initArray;
        }

        $colors = [];

        foreach (theme_config('tinymce_text_colors') as $name => $hax) {
            $colors[] = preg_replace('/[^\d]/', '', $hax);
            $colors[] = $name;
        }

        $initArray['textcolor_map'] = json_encode($colors);

        return $initArray;
    }

    protected function addEditorStyle($style)
    {
        if (! $this->isExternalSource($style)) {
            $style = resolve_config_url($style);

            if (! is_file($style['path'])) {
                return;
            }

            $style = $style['url'] . '?v=' . filemtime($style['path']);
        }

        add_editor_style(str_replace(',', '%2C', $style));
    }

    protected function isExternalSource($source)
    {
        return strpos($source, '//') === 0 || strpos($source, 'http') === 0;
    }
}
