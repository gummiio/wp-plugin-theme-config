<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class AdminQuickLink
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'addFrontPageLinks']);
        add_action('admin_menu', [$this, 'addBlogPageLinks']);
    }

    public function addFrontPageLinks()
    {
        if (! theme_config('admin.homepage_quick_link', true)) {
            return;
        }

        if (! $frontPage = get_option('page_on_front')) {
            return;
        }

        add_pages_page(
            'Homepage',
            'Homepage <span class="dashicons dashicons-migrate" style="font-size: 16px;"></span>',
            'edit_pages',
            str_replace(admin_url(), '', get_edit_post_link($frontPage))
        );
    }

    public function addBlogPageLinks()
    {
        if (! theme_config('admin.posts_quick_link', true)) {
            return;
        }

        if (! $postsPage = get_option('page_for_posts')) {
            return;
        }

        add_pages_page(
            'Posts Page',
            'Posts Page <span class="dashicons dashicons-migrate" style="font-size: 16px;"></span>',
            'edit_pages',
            str_replace(admin_url(), '', get_edit_post_link($postsPage))
        );
    }
}
