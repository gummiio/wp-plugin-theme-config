<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Debugger
{
    public $debugger;
    public $handler;
    protected $developmentCookieName = 'theme-builder-dev';

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'enableWhoops'], 0);
    }

    public function enableWhoops()
    {
        if ($this->isProduction()) {
            return;
        }

        if ($this->disableOnAjax()) {
            return;
        }

        if (theme_config('debug.force_reporting', true)) {
            error_reporting(theme_config('debug.reporting_level', E_ALL));
        }

        $this->debugger = new Run;
        $this->handler  = new PrettyPageHandler;
        $this->handler->setPageTitle(theme_config('debug.page_title', 'Oops...'));

        $this->debugger->pushHandler($this->handler);
        $this->debugger->register();
    }

    public function isProduction()
    {
        return ! $this->isDevelopment();
    }

    public function isDevelopment()
    {
        return theme_config('debug.enabled') || data_get($_COOKIE, $this->developmentCookieName, false);
    }

    protected function disableOnAjax()
    {
        $ajax = defined('DOING_AJAX') && DOING_AJAX;
        $disabled = theme_config('disable_on_ajax', true);

        return $ajax && $disabled;
    }
}
