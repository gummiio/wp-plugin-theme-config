<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class ThemeSupport
{
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'enableThemeSupports']);
    }

    public function enableThemeSupports()
    {
        if (! $supports = theme_config('theme_supports', ['post-thumbnails', 'title-tag'])) {
            return;
        }

        collect($supports)->each(function($value, $key) {
            $value = array_wrap($value);

            if (! is_numeric($key)) {
                array_unshift($value, $key);
            }

            call_user_func_array('add_theme_support', $value);
        });
    }
}
