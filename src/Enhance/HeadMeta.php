<?php

namespace Gummiforweb\ThemeBuilder\Enhance;

class HeadMeta
{
    public function __construct()
    {
        add_action('init', [$this, 'runCleanUp']);
    }

    public function runCleanUp()
    {
        // clean up
        $this->cleanUpInfo();
        $this->cleanUpFeeds();
        $this->cleanUpRelationalLinks();
        $this->cleanUpEmoji();
        $this->cleanUpOembed();
        $this->cleanUpRestApi();
        $this->cleanUpPrefetching();

        // utilities
        add_action('wp_head', [$this, 'disableFormatDetection']);
    }

    public function disableFormatDetection()
    {
        if (! $types = theme_config('wp_head.disable_format_detection')) return;

        if ($types === true || $types = '*') {
            $types = ['telephone', 'date', 'address', 'email'];
        }

        if (is_string($types)) {
            $types = array_map('trim', explode(',', $types));
        }

        echo collect($types)->map(function($type) {
            return sprintf('<meta name="format-detection" content="%s=no">', $type);
        })->implode("\n");
    }

    protected function cleanUpInfo()
    {
        remove_action('wp_head', 'rsd_link'); // Really Simple Discovery
        remove_action('wp_head', 'wlwmanifest_link'); // Windows Live Writer
        remove_action('wp_head', 'wp_generator'); // WordPress generator meta tag
    }

    protected function cleanUpFeeds()
    {
        if (! theme_config('wp_head.remove_rss_head_links')) return;

        remove_action('wp_head', 'feed_links', 2); // remove rss feed links
        remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
    }

    protected function cleanUpRelationalLinks()
    {
        if (! theme_config('wp_head.remove_relational_links')) return;

        remove_action('wp_head', 'rel_canonical');
        remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    }

    protected function cleanUpEmoji()
    {
        if (! theme_config('wp_head.remove_emoji')) return;

        remove_action('wp_head', 'print_emoji_detection_script', 7, 1);
        remove_action('wp_print_styles', 'print_emoji_styles', 10, 1);
    }

    protected function cleanUpOembed()
    {
        if (! theme_config('wp_head.remove_wp_oembed')) return;

        remove_action('wp_head', 'wp_oembed_add_discovery_links', 10, 1);
        remove_action('wp_head', 'wp_oembed_add_host_js', 10, 1);

    }

    protected function cleanUpRestApi()
    {
        if (! theme_config('wp_head.remove_rest_api')) return;

        remove_action('wp_head', 'rest_output_link_wp_head', 10, 0);
    }

    protected function cleanUpPrefetching()
    {
        if (! theme_config('wp_head.remove_prefetching')) return;

        remove_action('wp_head', 'wp_resource_hints', 2, 1);
    }
}
