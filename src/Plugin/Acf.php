<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

class Acf
{
    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'registerAcfOptionPages']);
        add_action('acf/init', [$this, 'modifyDefaultOptions']);
        add_filter('acf/init', [$this, 'maybeHideAdminMenu']);
        add_filter('acf/init', [$this, 'setupGoogleMapKey']);
    }

    public function registerAcfOptionPages()
    {
        if (! theme_config('acf.option_pages') || ! function_exists('acf_add_options_page')) {
            return;
        }

        acf_add_options_page();

        collect(theme_config('acf.option_pages'))->each(function($page) {
            acf_add_options_sub_page($page);
        });
    }

    public function modifyDefaultOptions()
    {
        if (! $defualts = theme_config('acf.field_type_defaults')) {
            return;
        }

        collect($defualts)->each(function($overwrite, $name) {
            if (! $field = acf_get_field_type($name)) {
                return true;
            }

            $field->defaults = wp_parse_args($overwrite, $field->defaults);
        });
    }

    public function maybeHideAdminMenu()
    {
        if (! theme_config('acf.show_admin_on_debug_only')) {
            return;
        }

        acf_update_setting('show_admin', theme_builder('debugger')->isDevelopment());
    }

    public function setupGoogleMapKey()
    {
        if (! $key = theme_config('acf.google_api_key')) {
            return;
        }

        acf_update_setting('google_api_key', $key);
    }
}
