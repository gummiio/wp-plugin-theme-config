<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

use Gummiforweb\ThemeBuilder\Traits\ReorderCommon;

class ReorderTerms
{
    use ReorderCommon;

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'initialize']);
    }

    public function initialize()
    {
        if (! $this->isReorderActivated() || ! $this->isReorderTermsActivated()) {
            return;
        }

        add_filter('metronet_reorder_post_types', [$this, 'appendReorderTermsPostTypes'], 15);

        add_filter('get_terms_defaults', [$this, 'removeDefaultOrdering']);

        // pre_get_terms too late, cause term query part tax query before pre_get_terms
        add_action('parse_term_query', [$this, 'setReorderFlag'], 20);
        add_action('parse_term_query', [$this, 'setOrderBy'], 50);
    }

    public function appendReorderTermsPostTypes($postTypes)
    {
        if (! $this->managedByConfig()) {
            return $postTypes;
        }

        return collect($postTypes)
            ->merge($this->getReorderTermsPostTypes())
            ->unique()
            ->values()
            ->all();
    }

    public function removeDefaultOrdering($defaults)
    {
        $this->defaultOrder = $defaults['order'];
        $this->defaultOrderBy = $defaults['orderby'];

        $defaults['order'] = '';
        $defaults['orderby'] = '';
        return $defaults;
    }

    public function setReorderFlag($query)
    {
        if (! $this->managedByConfig() || ! $this->needToSetReorder($query)) {
            $this->restoreDefaultOrdering($query);
            return;
        }

        $query->query_vars['reorder'] = true;
    }

    public function setOrderBy($query)
    {
        if (! data_get($query->query_vars, 'reorder')) {
            return;
        }

        if (! data_get($query->query_vars, 'meta_query')) {
            $query->query_vars['meta_query'] = [];
        }

        $postType = $this->getTaxonomyPrimaryPostType($this->getQueryTaxonomy($query));

        $query->query_vars['meta_query'][] = [
            'relation' => 'OR',
            [
                'key' => "{$postType}_order",
                'compare' => 'NOT EXISTS'
            ],
            [
                'key' => "{$postType}_order",
                'value' => 0,
                'compare' => '>=',
            ]
        ];

        // wp term query doesn't allow multiple order at the moment :/
        $query->query_vars['orderby'] = 'meta_value_num';
        $query->query_vars['order'] = 'ASC';
    }

    protected function getReorderTermsPostTypes()
    {
        return $this->getTaxonomyPostTypes($this->getReorderTermsTaxonomies());
    }

    protected function getReorderTermsTaxonomies()
    {
        if (theme_config('reorder_terms.set_on_taxonomies')) {
            return $this->filterByRegisteredTaxonomies();
        }

        return $this->filterByConfig();
    }

    protected function filterByRegisteredTaxonomies()
    {
        return collect(get_taxonomies([], 'object'))
            ->where(theme_config('reorder_terms.register_key', 'reorder'), true)
            ->pluck('name', 'name')
            ->all();
    }

    protected function filterByConfig()
    {
        return array_diff(
            $this->filterConfigValue(get_taxonomies(), 'reorder_terms.enabled_taxonomies', '*'),
            $this->filterConfigValue(get_taxonomies(), 'reorder_terms.disabled_taxonomies', [])
        );
    }

    protected function needToSetReorder($query)
    {
        if (data_get($query->query_vars, 'no_reorder')) {
            return false;
        }

        if ($this->isTrueAdmin()) {
            return false;
        }

        if ($this->hasMultipleTaxonomy($query)) {
            return false;
        }

        if ($this->hasTermOrderQuery($query)) {
            return false;
        }

        return in_array($this->getQueryTaxonomy($query), $this->getReorderTermsTaxonomies());
    }

    protected function restoreDefaultOrdering($query)
    {
        $query->query_vars['orderby'] = $this->defaultOrderBy;
        $query->query_vars['order'] = $this->defaultOrder;
    }

    protected function hasMultipleTaxonomy($query)
    {
        $taxonomies = array_wrap(data_get($query->query_vars, 'taxonomy', []));

        return count($taxonomies) > 1;
    }

    protected function hasTermOrderQuery($query)
    {
        $order = data_get($query->query_vars, 'order');
        $orderBy = data_get($query->query_vars, 'orderby');

        return $order || $orderBy;
    }

    protected function getQueryTaxonomy($query)
    {
        return collect(array_wrap(data_get($query->query_vars, 'taxonomy', [])))->first();
    }

    protected function getTaxonomyPrimaryPostType($taxonomy)
    {
        $taxonomy = get_taxonomy($taxonomy);
        return collect(array_wrap(data_get($taxonomy, 'object_type', [])))->first();
    }
}
