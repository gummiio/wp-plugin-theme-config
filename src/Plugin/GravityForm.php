<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

use GFAPI;

class GravityForm
{
    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'setupDefaultSetting']);
        add_action('plugins_loaded', [$this, 'maybeDisableViewCount']);
        add_filter('gform_form_list_columns', [$this, 'hideViewCountFromListTaable']);
        add_action('gform_after_save_form', [$this, 'enableHoneyPot'], 10, 2);
    }

    public function setupDefaultSetting()
    {
        if (! $this->pluginActivated()) {
            return;
        }

        $this->setOption('rg_gforms_disable_css', theme_config('gform.defaults.disable_css'));
        $this->setOption('rg_gforms_enable_html5', theme_config('gform.defaults.enable_html5'));
        $this->setOption('gform_enable_toolbar_menu', theme_config('gform.defaults.toolbar_menu'));
    }

    public function maybeDisableViewCount()
    {
        if (! $this->pluginActivated()) {
            return;
        }

        if (! $disable = theme_config('gform.disable_view_counter')) {
            return;
        }

        if ($disable === true) {
            add_filter('gform_disable_view_counter', '__return_true');
            return;
        }

        if (is_string($disable)) {
            $disable = array_map('trim', explode(',', $disable));
        }

        collect($disable)->each(function($formId) {
            add_filter("gform_disable_view_counter_{$formId}", '__return_true');
        });
    }

    public function hideViewCountFromListTaable($columns)
    {
        if (theme_config('gform.disable_view_counter') !== true) {
            return $columns;
        }

        unset($columns['view_count']);
        unset($columns['conversion']);

        return $columns;
    }

    public function enableHoneyPot($form, $isNew)
    {
        if (! theme_config('gform.enable_honeypot_on_new_form')) {
            return;
        }

        if (! $isNew) {
            return;
        }

        $form['enableHoneypot'] = true;
        $form['is_active'] = true; // have to set this for some reason

        GFAPI::update_form($form);
    }

    protected function pluginActivated()
    {
        return class_exists('GFCommon');
    }

    protected function setOption($option, $value)
    {
        if (is_null($value)) {
            return;
        }

        if (get_option($option) !== false) {
            return;
        }

        update_option($option, $value);
    }
}
