<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

use Gummiforweb\ThemeBuilder\Traits\ReorderCommon;

class Reorder
{
    use ReorderCommon;

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'initialize']);
    }

    public function initialize()
    {
        if (! $this->isReorderActivated()) {
            return;
        }

        add_filter('metronet_reorder_post_allow_admin', [$this, 'turnOffReorderAdmin']);
        add_filter('metronet_reorder_post_types', [$this, 'getReorderPostTypes']);

        add_action('pre_get_posts', [$this, 'setReorderFlag'], 20);
        add_action('pre_get_posts', [$this, 'setOrderBy'], 50);
    }

    public function turnOffReorderAdmin($auto)
    {
        if (! $this->managedByConfig()) {
            return $auto;
        }

        return false;
    }

    public function getReorderPostTypes($postTypes = [])
    {
        if (! $this->managedByConfig()) {
            return $postTypes;
        }

        if (theme_config('reorder.set_on_post_types')) {
            return $this->filterByRegisteredPostTypes();
        }

        return $this->filterByConfig();
    }

    public function setReorderFlag($query)
    {
        if (! $this->managedByConfig() || ! $this->needToSetReorder($query)) {
            return;
        }

        $query->set('reorder', true);
    }

    public function setOrderBy($query)
    {
        if (! $query->get('reorder')) {
            return;
        }

        $query->set('orderby', [
            'menu_order' => 'asc'
        ]);
    }

    protected function filterByRegisteredPostTypes()
    {
        return collect(get_post_types([], 'object'))
            ->where(theme_config('reorder.register_key', 'reorder'), true)
            ->pluck('name', 'name')
            ->all();
    }

    protected function filterByConfig()
    {
        return array_diff(
            $this->filterConfigValue(get_post_types(), 'reorder.enabled_post_types', '*'),
            $this->filterConfigValue(get_post_types(), 'reorder.disabled_post_types', [])
        );
    }

    protected function needToSetReorder($query)
    {
        if ($query->get('no_reorder')) {
            return false;
        }

        if ($this->isTrueAdmin() || $query->is_search()) {
            return false;
        }

        if ($this->hasOrderQuery($query)) {
            return false;
        }

        if ($this->hasMultiplePostTypes($query)) {
            return false;
        }

        if ($this->hasTaxonomyQuery($query)) {
            return false;
        }

        return in_array($this->getQueryPostType($query), $this->getReorderPostTypes());
    }

    protected function getQueryPostType($query)
    {
        return collect(array_wrap($query->get('post_type')))->first()? : 'post';
    }
}
