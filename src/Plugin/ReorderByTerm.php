<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

use Gummiforweb\ThemeBuilder\Traits\ReorderCommon;

class ReorderByTerm
{
    use ReorderCommon;

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'initialize']);
    }

    public function initialize()
    {
        if (! $this->isReorderActivated() || ! $this->isReorderByTermActivated()) {
            return;
        }

        add_filter('metronet_reorder_post_types', [$this, 'appendReorderTermsPostTypes'], 15);
        add_filter('reorder_term_build_get_taxonomies', [$this, 'getBuildToolTaxonomies']);

        add_action('pre_get_posts', [$this, 'setReorderByTermFlag'], 25);
        add_action('pre_get_posts', [$this, 'setOrderBy'], 55);
    }

    public function appendReorderTermsPostTypes($postTypes)
    {
        if (! $this->managedByConfig()) {
            return $postTypes;
        }

        $postTypes = collect($postTypes)->merge($this->getReorderByTermPostTypes());

        return $postTypes->unique()->values()->all();
    }

    public function getBuildToolTaxonomies($taxonomies)
    {
        if (! $this->managedByConfig()) {
            return $postTypes;
        }

        return $this->getReorderByTermTaxonomies();
    }

    public function setReorderByTermFlag($query)
    {
        if (! $this->managedByConfig() || ! $this->needToSetReorderByTerm($query)) {
            return;
        }

        $query->set('reorder_by_term', true);
    }

    public function setOrderBy($query)
    {
        if (! $query->get('reorder_by_term')) {
            return;
        }

        $taxonomy = $this->getQueryTaxonomy($query);
        $term = $this->getQueryTerm($query);

        $metaQuery = $query->get('meta_query')? : [];
        $metaQuery['term_reorder'] = [
            'key' => "_reorder_term_{$taxonomy}_{$term}",
            'type'    => 'NUMERIC',
            'compare' => 'exists'
        ];

        $query->set('meta_query', $metaQuery);

        $query->set('orderby', [
            'term_reorder' => 'asc'
        ]);
    }

    protected function getReorderByTermPostTypes()
    {
        return $this->getTaxonomyPostTypes($this->getReorderByTermTaxonomies());
    }

    protected function getReorderByTermTaxonomies()
    {
        if (theme_config('reorder_by_term.set_on_taxonomies')) {
            return $this->filterByRegisteredTaxonomies();
        }

        return $this->filterByConfig();
    }

    protected function filterByRegisteredTaxonomies()
    {
        return collect(get_taxonomies([], 'object'))
            ->where(theme_config('reorder_by_term.register_key', 'reorder_by_term'), true)
            ->pluck('name', 'name')
            ->all();
    }

    protected function filterByConfig()
    {
        return array_diff(
            $this->filterConfigValue(get_taxonomies(), 'reorder_by_term.enabled_taxonomies', '*'),
            $this->filterConfigValue(get_taxonomies(), 'reorder_by_term.disabled_taxonomies', [])
        );
    }

    protected function needToSetReorderByTerm($query)
    {
        if ($query->get('no_reorder_by_term')) {
            return false;
        }

        if ($this->isTrueAdmin() || $query->is_search()) {
            return false;
        }

        if ($this->hasOrderQuery($query)) {
            return false;
        }

        if ($this->hasMultiplePostTypes($query)) {
            return false;
        }

        if (! $this->isArchive($query)) {
            return false;
        }

        return in_array($this->getQueryTaxonomy($query), $this->getReorderByTermTaxonomies());
    }

    protected function isArchive($query)
    {
        return $query->is_main_query() && ($query->is_tax() || $query->is_category() || $query->is_tag());
    }

    protected function getQueryTaxonomy($query)
    {
        if ($query->get('cat')) {
            return 'category';
        }

        if ($query->get('tag')) {
            return 'post_tag';
        }

        return collect(get_taxonomies())->intersect(array_keys($query->query))->first();
    }

    protected function getQueryTerm($query)
    {
        if ($query->get('cat')) {
            return get_term($query->get('cat'))->slug;
        }

        if ($query->get('tag')) {
            return $query->get('tag');
        }

        return $query->query[$this->getQueryTaxonomy($query)];
    }
}
