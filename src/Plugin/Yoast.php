<?php

namespace Gummiforweb\ThemeBuilder\Plugin;

class Yoast
{
    public function __construct()
    {
        add_action('wpseo_metabox_prio', [$this, 'modifyYoastPriority']);
    }

    public function modifyYoastPriority($priority)
    {
        if (! theme_config('yoast.move_metabox_to_end')) {
            return $priority;
        }

        return 'low';
    }
}
