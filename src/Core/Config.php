<?php

namespace Gummiforweb\ThemeBuilder\Core;

class Config
{
    protected $config = [];

    public function __construct()
    {
        $this->loadTemplateConfig();
        $this->loadStylesheetConfig();
    }

    public function loadConfig($src = null, $append = true)
    {
        $config = $this->standardizeConfig($src);

        $this->config = $append ? array_replace_recursive($this->config, $config) : $config;
    }

    public function get($key, $default = null)
    {
        return data_get($this->config, $key, $default);
    }

    protected function loadTemplateConfig()
    {
        return $this->loadConfig(get_template_directory() . '/' . $this->getConfigFileName());
    }

    protected function loadStylesheetConfig()
    {
        return $this->loadConfig(get_stylesheet_directory() . '/' . $this->getConfigFileName());
    }

    protected function getConfigFileName()
    {
        return defined('THEME_HELPER_CONFIG_FILE')? THEME_HELPER_CONFIG_FILE : '.theme-config.php';
    }

    protected function standardizeConfig($src)
    {
        if (is_object($src) || is_array($src)) {
            return $src;
        }

        if (is_file($src)) {
            return require($src);
        }

        return [];
    }
}
