<?php

namespace Gummiforweb\ThemeBuilder\Core;

class ThemeUpdate
{
    public function __construct()
    {
        add_filter('wp_prepare_themes_for_js', [$this, 'disableThemeUpdate']);
    }

    public function disableThemeUpdate($themes)
    {
        if (! theme_config('disable_current_theme_update')) {
            return $themes;
        }

        $themes[wp_get_theme()->get_stylesheet()]['hasUpdate'] = false;

        return $themes;
    }
}
