<?php

namespace Gummiforweb\ThemeBuilder\Core;

class Updator
{
    protected $requestUrl = 'http://plugin-check.dev/get-info.php';
    protected $slug = 'theme-builder/index.php';

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'check_version']);
        add_filter('plugins_api', [$this, 'requestPluginApi'], 10, 3);
        add_filter('pre_set_site_transient_update_plugins', [$this, 'updatePlugins']);
    }

    public function check_version()
    {
        if (get_option('theme_builder_version') === theme_builder()->version()) return;

        // some update scripts needed to be run
    }

    public function requestPluginApi($false, $action, $args)
    {
        if ($action !== 'plugin_information') {
            return $false;
        }

        if ($args->slug != $this->slug) {
            return $false;
        }

        $response = $this->fetchPluginInfo(['action' => 'check_info']);

        return $response;
    }

    public function updatePlugins($transient)
    {
        if (empty($transient->checked)) {
            return $transient;
        }

        $plugin_path = plugin_basename(theme_builder()->file());


        return $transient;
    }

    protected function fetchPluginInfo($args = [])
    {
        $request = wp_remote_post($this->requestUrl, [
            'body' => wp_parse_args($args, [
                'apiKey' => get_option('theme_builder_api_key', '')
            ])
        ]);

        if (is_wp_error($request) || 200 != wp_remote_retrieve_response_code($request)) {
            return false;
        }

        $response = json_decode(wp_remote_retrieve_body($request));

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }

        return $response;
    }
}
