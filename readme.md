# WordPress Plugin: Theme Config (WIP)

Single config file to manage the common WordPress theme's common operation such as `register_post_type`, `add_image_size` etc...

## Sample Congif File
create `.theme-config.php` in the root of your theme.

```
<?php

return [
    // theme options
    'disable_current_theme_update' => true,

    // menus
    'menus' => [
        'primary' => 'Primary Menu',
        'footer'  => 'Footer Menu'
    ],

    // post types
    'custom_post_types' => [
        'service' => [
            'singular'  => 'Service',
            'plural'    => 'Services',
            'url_slug'  => 'our-services',
            'menu_icon' => 'dashicon-nametag'
        ],
    ],

    // taxonomies
    'custom_taxonomies' => [
        'service_type' => [
            'singular'   => 'Service Type',
            'plural'     => 'Service Types',
            'url_slug'   => 'service-types',
            'post_types' => 'service',
        ],
    ],

    // sizes
    'thumbnail_sizes' => [
        'screen-max'     => [1800, null],
        'screen-full'    => [1200, null],
        'screen-desktop' => [980, null],
        'screen-tablet'  => [768, null],
        'screen-mobile'  => [480, null],
    ],

    // scripts
    'favicon'     => 'images/favicon.png',
    'phone_icon'  => 'images/phone_icon.png',
    'typekit_id'  => 'abc123',
    'jquery'      => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js',
    'fontawesome' => 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
    'googlefonts' => 'https://fonts.googleapis.com/css?family=Dancing+Script|Nunito:400,600,600i,700,700i',
    'frontend_scripts' => [
        'slick'        => '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',
        'theme-script' => 'scripts/site-js.js',
    ],
    'frontend_styles' => [
        'slick'       => '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css',
        'theme-style' => 'styles/css/main-style.css',
    ],

    // admin
    'admin_logo' => '',
    'admin' => [
        'homepage_quick_link' => true,
        'posts_quick_link' => true,
    ],
    'admin_scripts' => [
        'theme-admin' => 'scripts/admin-js.js',
    ],
    'admin_styles' => [
        'theme-admin' => 'styles/css/admin-style.css',
    ],

    // editor
    'editor_styles' => [
        'theme-wysiwyg' => 'styles/css/wysiwyg.css',
    ],
    'enable_tinymce_additional_styles' => true,
    'tinymce_additional_styles' => [
        [
            'title'   => 'Intro Paragraph',
            'block'   => 'p',
            'classes' => 'intro',
            'wrapper' => false,
        ],
    ],
    'enable_tinymce_text_colors' => false,
    'tinymce_text_colors' => [
        'Black' => '262626',
        'Blue'  => '2f9ba8'
    ],

    'debug' => [
        'enabled'         => true,
        'disable_on_ajax' => true,
        'page_title'      => 'Ahh damn! Something is wrong...',
        'force_reporting' => true,
        'reporting_level' => E_ALL,
    ],

    // wp head
    'wp_head' => [
        'remove_rss_head_links'    => true,
        'remove_relational_links'  => true,
        'remove_emoji'             => true,
        'remove_wp_oembed'         => true,
        'remove_rest_api'          => true,
        'remove_prefetching'       => true,
        'disable_format_detection' => true,
        'disable_format_detection' => [],
    ],

    // theme support
    'theme_supports' => [
        'post-thumbnails',
        'title-tag',
        'html5'
    ],

    // acf
    'acf' => [
        'option_pages' => [
            'General',
            'Banner',
            'Callouts',
            'Footer',
        ],
        'show_admin_on_debug_only' => true,
        'google_api_key' => '',
        'field_type_defaults' => [
            'textarea' => [
                'rows'      => 5,
                'new_lines' => 'wpautop'
            ],
            'wysiwyg' => [
                'toolbar' => 'basic',
                'media_upload' => false
            ],
            'checkbox' => [
                'layout' => 'horizontal'
            ],
            'radio' => [
                'layout' => 'horizontal'
            ],
            'true_false' => [
                'ui' => true
            ],
            'taxonomy' => [
                'add_term'      => false,
                'return_format' => 'object'
            ],
            'tab' => [
                'placement' => 'left'
            ],
            'repeater' => [
                'layout' => 'block'
            ]
        ]
    ],

    // gravity form
    'gform' => [
        'defaults' => [
            'disable_css'  => true,
            'enable_html5' => true,
            'toolbar_menu' => true,
        ],
        'disable_view_counter'        => true,
        'enable_honeypot_on_new_form' => true
    ],

    // reorder plugin
    'reorder' => [
        'managed_by_config' => true,
        'set_on_post_types' => false,
        'register_key' => 'reorder',
        'enabled_post_types' => ['post', 'service'],
        // 'enabled_post_types' => 'post, service',
        // 'enabled_post_types' => '*',
        // 'disabled_post_types' => ['post'],
    ],

    'reorder_terms' => [
        'set_on_taxonomies' => false,
        'register_key' => 'reorder',
        'enabled_taxonomies' => ['service_type'],
        // 'disabled_taxonomies' => ['service_type'],
    ],

    'reorder_by_term' => [
        'set_on_taxonomies' => true,
        'register_key' => 'reorder_by_term',
        'enabled_taxonomies' => ['post_tag', 'service_type'],
        // 'enabled_taxonomies' => 'category, post_tag, service_type',
        // 'enabled_taxonomies' => '*',
        // 'disabled_taxonomies' => ['post_tag'],
    ],

    'yoast' => [
        'move_metabox_to_end' => true
    ]
];

```
