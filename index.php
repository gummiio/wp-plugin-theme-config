<?php
/*
    Plugin Name: Theme Config
    Version: 1.0.0
    Author: Alan Chen
    License: GPLv2 or later
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

require_once dirname(__FILE__) . '/vendor/autoload.php';

global $themeBuilder;
$themeBuilder = new Gummiforweb\ThemeBuilder\ThemeBuilder(__FILE__, '1.0.0');
